<?php

use lib\Blog\BlogDataRepository;
use lib\Blog\BlogDataService;
use lib\Communication\CommunicationHandler;
use lib\Configuration\ConfigParser;

require_once(dirname(__DIR__) . '/app/init.php');

ob_start();
echo 'Start removing old items...' . PHP_EOL;
flush();
ob_flush();


//number of weeks passed as argument to cron from cli
$weeksNumber = $argv[1];
if (empty($weeksNumber) || !is_numeric($weeksNumber)) {
    $config = new ConfigParser();
    $weeksNumber = $config->getConfigBySection(ConfigParser::BLOG_KEY)['itemsToBeRemovedAfter'];
    echo 'Incorrect parameter sent, automatically sent to default interval: ' . $weeksNumber . PHP_EOL;
    flush();
    ob_flush();
}

//instantiate service class to get blog data from server
$communicationHandler = new CommunicationHandler();
$blogDataRepository = new BlogDataRepository();
$blogDataService = new BlogDataService($communicationHandler, $blogDataRepository);

$blogDataService->removeOlderData($weeksNumber);

echo 'Finish removing data!'. PHP_EOL;
flush();
ob_end_flush();