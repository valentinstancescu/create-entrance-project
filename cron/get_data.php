<?php

use lib\Blog\BlogDataRepository;
use lib\Blog\BlogDataService;
use lib\Communication\CommunicationHandler;

require_once(dirname(__DIR__) . '/app/init.php');

ob_start();
echo 'Start processing RDF file...' . PHP_EOL;
flush();
ob_flush();

//instantiate service class to get blog data from server
$communicationHandler = new CommunicationHandler();
$blogDataRepository = new BlogDataRepository();
$blogDataService = new BlogDataService($communicationHandler, $blogDataRepository);


try{
    //call method to process data
    $blogItems = $blogDataService->getDataFromServer();

    //iterate over generator
    /** @var \lib\Blog\Entity\BlogItem $item */
    foreach ($blogItems as $item) {
        echo 'Stored data for: ' . $item->getUrl() . PHP_EOL;
        flush();
        ob_flush();
        //sleep(1); //if you want to see on display how cron is processing, but performance will be affected
    }

} catch (\lib\Blog\BlogDataServiceException $exception) {
    echo $exception->getMessage();
    exit();
}

echo 'Finish saving data from server!'. PHP_EOL;
flush();
ob_end_flush();
