<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create entrance project</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style type="text/css">
        body {  font-size: medium; font-family: Tahoma, Arial, serif; }
    </style>
</head>
<body>

    <div class="container">
        <h1>Welcome to Valentin's project!</h1>
        <div id="errorMessage" style="padding: 10px 0px; margin: 10px 0px; color: #dc0000;">{$errorMessage}</div>
        <form action="" method="get" id="search_form" class="form-signin">
            <input type="text" name="user_name" id="user_name" value="{$userName}" placeholder="User Name" class="form-control" onfocus="document.getElementById('curent_page').value = 1;" /><br/><br/>
            <input type="text" name="server_name" id="server_name" value="{$serverName}" placeholder="Server Name" class="form-control" onfocus="document.getElementById('curent_page').value = 1;" /><br/><br/>
            <input type="text" name="entry_number" id="entry_number" placeholder="Entry Number" class="form-control" value="{$entryNumber}" onfocus="document.getElementById('curent_page').value = 1;" /><br/><br/>
            <input type="text" name="date" id="date" value="{$date}" placeholder="Date" class="form-control" onfocus="document.getElementById('curent_page').value = 1;" /><br/><br/>
            <input type="hidden" name="current_page" id="curent_page" value="{$currentPage + 1}" />
            <input type="submit" value="Show items" class="btn btn-lg btn-primary btn-block" onkeypress="document.getElementById('curent_page').value = 1;" onmouseenter="document.getElementById('curent_page').value = 1;" />
        </form>
    </div>
    <div class="container">
        <div class="row">
            <div style="float: right;" class="pagination">
                {if $currentPage > 1}
                    <a href="javascript:void();" onclick="document.getElementById('curent_page').value = {$currentPage - 1};document.getElementById('search_form').submit();">Previous Page</a>
                {/if}
                Page: {$currentPage} of: {$totalPages}
                {if $currentPage < $totalPages}
                    <a href="javascript:void();" onclick="document.getElementById('search_form').submit();">Next Page</a>
                {/if}
                 / Total items: {$totalDataRecords}
            </div>
        </div>


        <div class="list-group">
        {if empty($filteredData)} No data found {/if}
        {foreach $filteredData as $item}
            <div id="{$item->getItemId()}" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1" style="float: left; font-weight: bold">{$item->getTitle()}</h5>
                    <small class="text-muted float-right"  style="float: right;">{$item->getDate()|date_format:"%d.%m.%Y, %H:%M:%S"}</small>
                </div>
                <p class="mb-1" style="clear: both">{$item->getDescription()}</p>
                <small class="text-muted"><a href="{$item->getUrl()}" target="_blank" rel="nofollow">{$item->getUrl()}</a></small>
            </div>
        {/foreach}
        </div>
    </div>
</body>
</html>