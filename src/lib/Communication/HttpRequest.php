<?php
namespace lib\Communication;

use Exception;

/**
 * Request representation. It provides method to store and interact with the request (url, method, headers, get and post data)
 * The request can be created and there are methods for interaction with:
 * 	- headers: setHeaders, getHeader, addHeader, removeHeader
 * 	- queryString (GET): setQueryString, getQueryString, getQueryValue, appendQueryVar, removeQueryVar
 * 	- postData (POST): setPostData, getPostData, getPostValue, appendPostVar, removePostVar
 */
class HttpRequest
{

    const GET     = 'GET';
    const POST    = 'POST';

    /** @var string */
    private $method;

    /** @var string */
    private $url;

    /** @var array */
    private $headers = [];

    /** @var string */
    private $queryString;

    /** @var mixed */
    private $postData;

    /** @var string */
    private $realSentHeadersStr = '';

    /** @var array */
    private $realSentHeadersArray = [];

    /**
     * HttpRequest constructor.
     * @param string $method
     * @throws Exception
     */
    public function __construct($method = self::GET)
    {
        switch ($method) {
            case self::GET:
            case self::POST:
                $this->method = $method;
                break;
            default:
                throw new Exception("Method {$method} not implemented.");
        }
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getUrlWithQueryString()
    {
        if (empty($this->queryString)) {
            return $this->url;
        }
        return $this->url . '?' . $this->queryString;
    }

    /**
     * @param $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return array
     */
    public function getHeaderValues()
    {
        $headers = [];
        foreach ($this->headers as $header) {
            $headerLine = explode(': ', $header);

            if ($headerLine[0]) {
                $headers[$headerLine[0]] = $headerLine[1];
            }
        }

        return $headers;
    }

    /**
     * @param $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getRealSentHeadersAsStr()
    {
        return $this->realSentHeadersStr;
    }

    /**
     * @return array
     */
    public function getRealSentHeadersAsArray()
    {
        return $this->realSentHeadersArray;
    }

    /**
     * @param string $headersStr
     */
    public function setRealSentHeaders($headersStr)
    {
        $this->realSentHeadersStr = $headersStr;
        $this->realSentHeadersArray = [];

        $headersArray = explode("\r\n", $headersStr);

        foreach ($headersArray as $header) {
            if (!empty($header)) {
                $this->realSentHeadersArray[] = $header;
            }
        }
    }

    /**
     * @param $header
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;
    }

    /**
     * @param $headerType
     * @return array
     */
    public function removeHeader($headerType)
    {
        $removedHeaders = [];
        foreach ($this->headers as $headerPosition => $header) {

            if (strtolower(substr($header, 0, strpos($header, ':'))) == strtolower($headerType)){
                $removedHeaders[] = $header;
                unset($this->headers[$headerPosition]);
            }
        }

        $this->headers = array_values($this->headers);

        return $removedHeaders;
    }

    /**
     * @param $headerTypes
     * @return array
     */
    public function removeHeaders($headerTypes)
    {
        $removedHeaders = [];
        foreach ($headerTypes as $headerType) {
            $stepRemovedHeaders = $this->removeHeader($headerType);
            $removedHeaders = array_merge($removedHeaders, $stepRemovedHeaders);
        }
        return $removedHeaders;
    }

    /**
     * @param string $queryString
     */
    public function setQueryString($queryString)
    {
        $this->queryString = $queryString;
    }

    /**
     * @return string
     */
    public function getQueryString()
    {
        return $this->queryString;
    }

    /**
     * @return array
     */
    public function getQueryArray()
    {
        return explode('&', $this->queryString);
    }

    /**
     * @return array
     */
    public function getQueryArrayParsed()
    {
        $result = [];
        foreach ($this->getQueryArray() as $vars) {
            $result[] = $this->_splitVarDecode($vars);
        }

        return $result;
    }

    /**
     * @param string|null $key
     * @return array|mixed|null
     */
    public function getQueryValue($key = null)
    {
        $result = [];
        foreach ($this->getQueryArrayParsed() as $keyValue) {
            if (!isset($key) || $keyValue[0] == $key) {
                $result[] = $keyValue[1];
            }
        }
        if (count($result) < 1) {

            return null;
        }
        if (count($result) == 1) {

            return reset($result);
        }

        return $result;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function appendQueryVar($key, $value)
    {
        $keyValueString = $key . '=' . urlencode($value);
        $this->setQueryString(implode('&', array_merge($this->getQueryArray(), array($keyValueString))));
    }

    /**
     * @param string $key
     * @return array|mixed|null
     */
    public function removeQueryVar($key)
    {
        $foundValues = $this->getQueryValue($key);
        $queryArray = $this->getQueryArray();

        foreach ($queryArray as $varPosition => $vars) {
            $keyValue = $this->_splitVarDecode($vars);
            if ($keyValue[0] == $key) {
                unset($queryArray[$varPosition]);
            }
        }

        $queryArray = array_values($queryArray);
        $this->setQueryString(implode('&', $queryArray));

        return $foundValues;
    }

    /**
     * @return mixed
     */
    public function getPostData()
    {
        return $this->postData;
    }

    /**
     * @param $post
     * @throws Exception
     */
    public function setPostData($post)
    {
        if ($this->method != self::POST) {
            throw new Exception('Cannot set post variable for a not-POST-like request.');
        }
        $this->postData = $post;
    }

    /**
     * @return array
     */
    public function getPostArray()
    {
        return explode('&', $this->postData);
    }

    /**
     * @return array
     */
    public function getPostArrayParsed()
    {
        $result = [];
        foreach ($this->getPostArray() as $vars) {
            $result[] = $this->_splitVarDecode($vars);
        }
        return $result;
    }

    /**
     * @param string|null $key
     * @return array|mixed|null
     */
    public function getPostValue($key = null)
    {
        $result = [];
        foreach ($this->getPostArrayParsed() as $keyValue) {
            if (!isset($key) || $keyValue[0] == $key) {
                $result[] = $keyValue[1];
            }
        }
        if (count($result) < 1) {
            return null;
        }
        if (count($result) == 1) {
            return reset($result);
        }
        return $result;
    }

    /**
     * @param string $key
     * @param string $value
     * @throws Exception
     */
    public function appendPostVar($key, $value)
    {
        $keyValueString = $key . '=' . urlencode($value);
        $this->setPostData(implode('&', array_merge($this->getPostArray(), array($keyValueString))));
    }


    /**
     * @param string $key
     * @return array|mixed|null
     * @throws Exception
     */
    public function removePostVar($key)
    {
        $foundValues = $this->getPostValue($key);
        $postArray = $this->getPostArray();

        foreach ($postArray as $varPosition => $vars) {
            $keyValue = $this->_splitVarDecode($vars);

            if ($keyValue[0] == $key) {
                unset($postArray[$varPosition]);
            }
        }

        $postArray = array_values($postArray);
        $this->setPostData(implode('&', $postArray));

        return $foundValues;
    }

    /**
     * @param $vars
     * @return array
     */
    private function _splitVarDecode($vars)
    {
        $keyValue = explode('=', $vars, 2);
        if (count($keyValue) == 1) {
            array_push($keyValue, null);
        }
        else {
            $keyValue[1] = urldecode($keyValue[1]);
        }

        return $keyValue;
    }
}
