<?php
namespace lib\Communication;

/**
 * Response representation. It provides method to store and interact with the response (http response code, headers and body)
 */
class HttpResponse
{

    private $responseCode;
    private $headers = [];
    private $body;
    private $curlError;
    private $curlErrorNo;
    private $debugInfo;

    /**
     * @return mixed
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * @param mixed $responseCode
     */
    public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /** @param array $headers */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @param $header
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getCurlError()
    {
        return $this->curlError;
    }

    /**
     * @param mixed $curlError
     */
    public function setCurlError($curlError)
    {
        $this->curlError = $curlError;
    }

    /**
     * @return mixed
     */
    public function getCurlErrorNo()
    {
        return $this->curlErrorNo;
    }

    /**
     * @param mixed $curlErrorNo
     */
    public function setCurlErrorNo($curlErrorNo)
    {
        $this->curlErrorNo = $curlErrorNo;
    }

    /**
     * @return mixed
     */
    public function getDebugInfo()
    {
        return $this->debugInfo;
    }

    /**
     * @param mixed $debugInfo
     */
    public function setDebugInfo($debugInfo)
    {
        $this->debugInfo = $debugInfo;
    }

    /**
     * @return array
     */
    public function getHeaderValues()
    {
        $headers = [];

        foreach ($this->headers as $header) {
            $headerLine = explode(': ', $header);
            if ($headerLine[0]) {
                $headers[$headerLine[0]] = $headerLine[1];
            }
        }

        return $headers;
    }

    /**
     * @param $headerType
     * @return array
     */
    public function removeHeader($headerType)
    {
        $removedHeaders = [];
        foreach ($this->headers as $headerPosition => $header) {
            if (strtolower(substr($header, 0, strpos($header, ':'))) == strtolower($headerType)){
                $removedHeaders[] = $header;
                unset($this->headers[$headerPosition]);
            }
        }
        $this->headers = array_values($this->headers);
        return $removedHeaders;
    }

    /**
     * @param $headerTypes
     * @return array
     */
    public function removeHeaders($headerTypes)
    {
        $removedHeaders = [];
        foreach ($headerTypes as $headerType) {
            $stepRemovedHeaders = $this->removeHeader($headerType);
            $removedHeaders = array_merge($removedHeaders, $stepRemovedHeaders);
        }
        return $removedHeaders;
    }

}
