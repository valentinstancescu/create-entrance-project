<?php

namespace lib\Communication;

/**
 * Class CommunicationHandler - wrapper class to be used in services in order to perform different HTTP requests
 */
class CommunicationHandler
{

    /** @var HttpClient */
    private $httpClient;

    /**
     * CommunicationHandler constructor.
     */
    public function __construct()
    {
        $this->httpClient = new HttpClient();
    }


    /**
     * Performs a GET request and returns the reponse body
     *
     * @param string $url
     * @param string $queryString
     *
     * @return string
     * @throws HttpException
     */
    public function performGetRequest($url, $queryString)
    {
        try{
            $httpRequest = new HttpRequest(HttpRequest::GET);
            $httpRequest->setUrl($url);
            $httpRequest->setQueryString($queryString);
        } catch (\Exception $exception) {
            throw new HttpException('HTTP Method not allowed!', 4005);
        }

        try {
            $httpResponse = $this->httpClient->call($httpRequest);
        } catch (HttpException $exception) {
            //this is a hack because on provided server cUrl exception is not loaded an I wasn't able to install it
            if ($exception->getCode() === HttpClient::CURL_NOT_INSTALLED_EXCEPTION_CODE) {
                return file_get_contents($url);
            }
        }


        if ($httpResponse->getCurlErrorNo() || !in_array($httpResponse->getResponseCode(), [200, 301, 302])) {
            throw new HttpException(
                print_r(
                    [
                        'Response' => $httpResponse->getBody(),
                        'HttpCode' => $httpResponse->getResponseCode(),
                        'CurlErrorNumber' => $httpResponse->getCurlErrorNo(),
                        'CurlError' => $httpResponse->getCurlError(),
                        'URL' => $url
                    ]),
                $httpResponse->getCurlErrorNo()
            );
        }

        return $httpResponse->getBody();
    }

}
