<?php

namespace lib\Communication;

/**
 * HttpClient representation that makes use of curl library to perform http requests
 */
class HttpClient
{

    const CURL_NOT_INSTALLED_EXCEPTION_CODE = 7000;

    private $curl;

    /** @var array */
    private $headers = [];

    /** @var string  */
    private $body = '';

    /** @var array */
    private $curlOptions = [];

    /** @var mixed */
    private $proxy;

    /** @var mixed */
    private $proxyUserPwd;

    /**
     * @param int $option
     * @param mixed $value
     *
     * @return $this
     */
    public function setCurlOption($option, $value)
    {
        $this->curlOptions[$option] = $value;

        return $this;
    }

    /**
     * @return $this
     */
    public function resetCurlOptions()
    {
        $this->curlOptions = [];

        return $this;
    }

    /**
     * @param HttpRequest $request
     * @throws HttpException
     * @return HttpResponse
     */
    public function call(HttpRequest $request)
    {
        $this->headers = [];
        $this->body = '';

        $this->init();
        $this->setDefaultCurlOptions();
        $this->setCustomCurlOptions();

        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $request->getMethod());
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $request->getHeaders());
        curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($this->curl, CURLOPT_URL, $request->getUrlWithQueryString());

        if (HttpRequest::POST == $request->getMethod()) {
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $request->getPostData());
        }

        curl_exec($this->curl);

        $responseCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        $realRequestHeaders = curl_getinfo($this->curl, CURLINFO_HEADER_OUT);

        $response = new HttpResponse();

        $response->setResponseCode($responseCode);
        $request->setRealSentHeaders($realRequestHeaders);

        $response->setHeaders($this->headers);
        $response->setBody($this->body);

        $response->setCurlError(curl_error($this->curl));
        $response->setCurlErrorNo(curl_errno($this->curl));

        $this->close();

        return $response;
    }

    /**
     * @throws HttpException
     */
    private function init()
    {
        if  (!in_array  ('curl', get_loaded_extensions())) {
            throw new HttpException('curl extension not loaded on server', self::CURL_NOT_INSTALLED_EXCEPTION_CODE);
        }
        $this->curl = curl_init();
    }

    private function close()
    {
        curl_close($this->curl);
    }

    private function setDefaultCurlOptions()
    {
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($this->curl, CURLOPT_HEADERFUNCTION, [$this, 'processHeader']);
        curl_setopt($this->curl, CURLOPT_WRITEFUNCTION, [$this, 'processBody']);
        curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, []);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);

        if (!empty($this->proxy)) {
            curl_setopt($this->curl, CURLOPT_PROXY, $this->proxy);

            if (!empty($this->proxyUserPwd)) {
                curl_setopt($this->curl, CURLOPT_PROXYUSERPWD, $this->proxyUserPwd);
            }
        }
    }

    private function setCustomCurlOptions()
    {
        foreach ($this->curlOptions as $option => $value) {
            curl_setopt($this->curl, $option, $value);
        }
    }

    private function processHeader($curl, $headerStr)
    {
        $header = trim($headerStr, "\r\n");

        if (!empty($header)) {
            $this->headers[] = $header;
        }

        return strlen($headerStr);
    }

    private function processBody($curl, $body)
    {
        $this->body .= $body;

        return strlen($body);
    }

}
