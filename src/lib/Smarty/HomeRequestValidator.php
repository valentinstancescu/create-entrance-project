<?php

namespace lib\Smarty;

use DateTime;

/**
 * Class HomeRequestValidator - validates request received from user
 */
class HomeRequestValidator
{
    const ERROR_ORDER_DATETIME = 'Invalid Date field. Should be in format: Y-m-d';
    const ERROR_INTERGER = '%s should be a number';

    /**
     * Validate that user has inputed correct parameters
     *
     * @throws \InvalidArgumentException
     * @param array $request
     */
    public function validateRequest($request)
    {
        if (!empty($request['server_name'])) {
            $this->isIntegerNumber($request['server_name']);
        }

        if (!empty($request['entry_number'])) {
            $this->isIntegerNumber($request['entry_number']);
        }

        if(!empty($request['date'])) {
            $this->isValidateDate($request['date']);
        }

        if(!empty($request['current_page'])) {
            $this->isIntegerNumber($request['current_page']);
        }
    }

    /**
     * Check if a value is an integer number even if it comes as string type
     *
     * @param $value
     * @throws \InvalidArgumentException
     * @return bool
     */
    private function isIntegerNumber($value)
    {
        if (is_int($value)
            || (is_string($value) && $value === (string)(int)$value)
            || (int)$value > 0
        ) {
            return true;
        }

        throw new \InvalidArgumentException(sprintf(self::ERROR_INTERGER, $value));
    }

    private function isValidateDate($orderDateTimeString)
    {
        if (!is_string($orderDateTimeString)) {
            throw new \InvalidArgumentException(self::ERROR_ORDER_DATETIME);
        }

        $orderDateTime = DateTime::createFromFormat('Y-m-d', $orderDateTimeString);

        if ($orderDateTime === false
            || (int)$orderDateTime->format('Y') <= 0
        ) {
            throw new \InvalidArgumentException(self::ERROR_ORDER_DATETIME);
        }
    }

}