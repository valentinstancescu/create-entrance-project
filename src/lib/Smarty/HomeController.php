<?php

namespace lib\Smarty;

use lib\Blog\BlogDataFilter;
use lib\Blog\BlogDataFilterBuilder;
use lib\Blog\BlogDataRepository;
use lib\Blog\BlogDataService;
use lib\Communication\CommunicationHandler;

/**
 * Class HomeController - class that orchestrates frontend operations
 */
class HomeController
{
    const TEMPLATE_FILE = 'home.tpl';
    const FILTER_COOKIE_NAME = 'CREATE-ENTRANCE';

    /** @var \Smarty */
    private $smartyEngine;

    /** @var BlogDataService */
    private $blogDataService;

    /** @var BlogDataFilterBuilder */
    private $blogDataFilterBuilder;

    /** @var HomeRequestValidator */
    private $homeRequestValidator;

    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $smartyTemplate = new SmartyFactory();
        $this->smartyEngine = $smartyTemplate::getTemplatEngine();

        $communicationHandler = new CommunicationHandler();
        $blogDataRepository = new BlogDataRepository();
        $this->blogDataService = new BlogDataService($communicationHandler, $blogDataRepository);
        $this->blogDataFilterBuilder = new BlogDataFilterBuilder();
        $this->homeRequestValidator = new HomeRequestValidator();
    }

    /**
     * @param array|null $serverGetRequest
     *
     * @return void;
     */
    public function display(array $serverGetRequest = [])
    {

        try {
            $this->homeRequestValidator->validateRequest($serverGetRequest);
        } catch (\InvalidArgumentException $exception) {
            $this->smartyEngine->assign('errorMessage', $exception->getMessage());
            $serverGetRequest = [];
        }

        $filterOptions = $this->blogDataFilterBuilder->build($serverGetRequest);
        $filteredData = $this->blogDataService->getFilteredData($filterOptions);
        $totalDataRecords = $this->blogDataService->getTotalDataRecords($filterOptions);


        $totalPages = ceil($totalDataRecords / BlogDataFilter::ITEMS_PER_PAGE);

        $this->smartyEngine->assign('filteredData', $filteredData);
        $this->smartyEngine->assign('totalDataRecords', $totalDataRecords);
        $this->smartyEngine->assign('totalPages', $totalPages);
        $this->smartyEngine->assign('recordsPerPage', BlogDataFilter::ITEMS_PER_PAGE);
        $this->smartyEngine->assign('currentPage', (!empty($serverGetRequest['current_page'])) ? $serverGetRequest['current_page'] : 1);
        $this->smartyEngine->assign('userName', $serverGetRequest['user_name']);
        $this->smartyEngine->assign('serverName', $serverGetRequest['server_name']);
        $this->smartyEngine->assign('entryNumber', $serverGetRequest['entry_number']);
        $this->smartyEngine->assign('date', $serverGetRequest['date']);
        
        try {
            $this->smartyEngine->display(self::TEMPLATE_FILE);
        } catch (\Exception $exception) {
            //nothing to show, smarty exception
        }

        return;
    }

    /**
     * @param string $cookie
     *
     * @return array
     */
    public function decodeCookieContent($cookie)
    {
        return json_decode(base64_decode($cookie), true);
    }

    /**
     * @param array $request
     *
     * @return void;
     */
    public function setCookie(array $request)
    {
        $cookieContent = base64_encode(json_encode($request));
        $expireTime = time() + 60 * 60 * 24 * 10; //cookie expires after 10 days

        setcookie(self::FILTER_COOKIE_NAME, $cookieContent, $expireTime, '/');
        return;
    }


}