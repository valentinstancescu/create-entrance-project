<?php

namespace lib\Blog;

/**
 * Class BlogDataFilter - value object class for filter data
 */
class BlogDataFilter
{
    const ITEMS_PER_PAGE = 10;

    /** @var string */
    private $userName;

    /** @var integer */
    private $serverName;

    /** @var integer */
    private $entryNumber;

    /** @var string */
    private $date;

    /** @var int */
    private $currentPage = 0;

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return int
     */
    public function getServerName()
    {
        return $this->serverName;
    }

    /**
     * @param int $serverName
     */
    public function setServerName($serverName)
    {
        $this->serverName = $serverName;
    }

    /**
     * @return int
     */
    public function getEntryNumber()
    {
        return $this->entryNumber;
    }

    /**
     * @param int $entryNumber
     */
    public function setEntryNumber($entryNumber)
    {
        $this->entryNumber = $entryNumber;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
    }

}