<?php

namespace lib\Blog;

/**
 * Class BlogDataFilterBuilder - build a filter class need to extract data from database
 */
class BlogDataFilterBuilder
{
    /**
     * @param $requestedData
     * @return BlogDataFilter
     */
    public function build($requestedData)
    {
        $blogDataFilter = new BlogDataFilter();

        if (!empty($requestedData['user_name'])) {
            $blogDataFilter->setUserName($requestedData['user_name']);
        }

        if (!empty($requestedData['server_name'])) {
            $blogDataFilter->setServerName($requestedData['server_name']);
        }

        if (!empty($requestedData['entry_number'])) {
            $blogDataFilter->setEntryNumber($requestedData['entry_number']);
        }

        if (!empty($requestedData['date'])) {
            $blogDataFilter->setDate($requestedData['date']);
        }

        if (!empty($requestedData['current_page'])) {
            $blogDataFilter->setCurrentPage($requestedData['current_page'] - 1);
        }

        return $blogDataFilter;
    }

}
