<?php

namespace lib\Blog;

/**
 * Class BlogData - value object class used to store data parsed from RDF file
 */
class BlogData
{
    /** @var  string */
    private $url;

    /** @var  string */
    private $title;

    /** @var  string */
    private $description;

    /** @var  string */
    private $date;

    /**
     * BlogData constructor.
     * @param string $url
     * @param string $title
     * @param string $description
     * @param string $date
     */
    public function __construct($url, $title, $description, $date)
    {
        $this->url = $url;
        $this->title = $title;
        $this->description = $description;
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

}
