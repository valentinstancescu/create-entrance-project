<?php

namespace lib\Blog;

/**
 * Class BlogDataParser - parse received data from server into proper object
 */
class BlogDataParser
{

    /** @var \DOMDocument */
    private $dom;

    public function __construct()
    {
        $this->dom = new \DOMDocument();
    }

    /**
     * @param $blogData
     * @return \Generator|BlogData
     */
    public function parseBlogData($blogData)
    {
        $this->dom->loadXML($blogData);

        //loop over xml nodes to find 'item' node
        foreach ($this->dom->childNodes as $childNode) {
            if ($childNode->nodeType === XML_ELEMENT_NODE) {
                $xmlItems = $childNode->childNodes;
                foreach ($xmlItems as $xmlItem) {
                    /** take just xml node with name item */
                    /** @var \DOMElement $xmlItem */
                    if ($xmlItem->nodeType === XML_ELEMENT_NODE && $xmlItem->nodeName === 'item') {

                        $elementDateTag = \DateTime::createFromFormat(\DateTime::ATOM, $xmlItem->getElementsByTagName('date')->item(0)->nodeValue);

                        //using yeild in order to go step by step and not to put the whole data into an array that at large amount of data could cause memory problems.
                        yield new BlogData(
                            $xmlItem->getElementsByTagName('link')->item(0)->nodeValue,
                            $xmlItem->getElementsByTagName('title')->item(0)->nodeValue,
                            $xmlItem->getElementsByTagName('description')->item(0)->nodeValue,
                            $elementDateTag->format('Y-m-d H:i:s')
                        );
                    }
                }
            }
        }

    }

}
