<?php

namespace lib\Blog\Entity;

/**
 * Class BlogItem - entity class which maps the database columns into code
 */
class BlogItem
{
    /** @var integer */
    private $itemId;

    /** @var string */
    private $userName;

    /** @var integer */
    private $serverName;

    /** @var integer */
    private $entryNumber;

    /** @var string */
    private $url;

    /** @var string */
    private $title;

    /** @var string */
    private $description;

    /** @var string */
    private $date;

    /**
     * BlogItem constructor.
     * @param string $userName
     * @param int $serverName
     * @param int $entryNumber
     * @param string $url
     * @param string $title
     * @param string $description
     * @param string $date
     */
    public function __construct($userName, $serverName, $entryNumber, $url, $title, $description, $date)
    {
        $this->userName = $userName;
        $this->serverName = $serverName;
        $this->entryNumber = $entryNumber;
        $this->url = $url;
        $this->title = $title;
        $this->description = $description;
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @param $itemId
     * @return $this
     */
    public function withItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return int
     */
    public function getServerName()
    {
        return $this->serverName;
    }

    /**
     * @return int
     */
    public function getEntryNumber()
    {
        return $this->entryNumber;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

}
