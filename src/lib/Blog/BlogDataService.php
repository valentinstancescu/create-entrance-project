<?php

namespace lib\Blog;

use lib\Communication\CommunicationHandler;
use lib\Communication\HttpException;
use lib\Configuration\ConfigParser;

/**
 * Class BlogDataService - provides methods to handle blog data
 */
class BlogDataService
{

    /** @var CommunicationHandler */
    private $communicationHandler;

    /** @var BlogDataRepository */
    private $blogDataRepository;

    /** @var array */
    private $blogConfig;

    /** @var  BlogDataParser */
    private $blogDataParser;

    /** @var BlogItemBuilder */
    private $blogItemBuilder;

    /**
     * BlogDataService constructor.
     * @param CommunicationHandler $communicationHandler
     * @param BlogDataRepository $blogDataRepository
     */
    public function __construct(
        CommunicationHandler $communicationHandler,
        BlogDataRepository $blogDataRepository
    ) {
        $configParser = new ConfigParser();

        $this->communicationHandler = new CommunicationHandler();
        $this->blogDataRepository = new BlogDataRepository();
        $this->blogDataParser = new BlogDataParser();
        $this->blogItemBuilder = new BlogItemBuilder();
        $this->blogConfig = $configParser->getConfigBySection(ConfigParser::BLOG_KEY);
    }

    /**
     * Performs a server request to get blog items and then store parsed item in proper format to database
     *
     * @return \Generator
     * @throws BlogDataServiceException
     */
    public function getDataFromServer()
    {
        try {
            $blogData = $this->communicationHandler->performGetRequest($this->blogConfig['url'], '');
        } catch (HttpException $exception) {
            throw new BlogDataServiceException($exception->getMessage(), $exception->getCode());
        }

        //parse and store data, and yeild a generator to each step
        foreach ($this->blogDataParser->parseBlogData($blogData) as $blogParsedItem) {
            $blogItem = $this->blogItemBuilder->buildBlogItem($blogParsedItem);

            if ($this->blogDataRepository->insertBlogData($blogItem)) {
                yield $blogItem;
            }
        }

    }

    /**
     * @param string $weeksNumber
     * @return boolean
     */
    public function removeOlderData($weeksNumber)
    {
        return $this->blogDataRepository->removeOlderData($weeksNumber);
    }


    /**
     * @param BlogDataFilter $filterOptions
     * @return Entity\BlogItem[]
     */
    public function getFilteredData(BlogDataFilter $filterOptions)
    {
        return $this->blogDataRepository->getFilteredData($filterOptions);
    }

    /**
     * @param BlogDataFilter $filterOptions
     * @return int
     */
    public function getTotalDataRecords(BlogDataFilter $filterOptions)
    {
        return $this->blogDataRepository->getTotalNumberOfRecords($filterOptions);
    }
}
