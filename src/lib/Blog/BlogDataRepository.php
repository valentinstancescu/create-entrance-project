<?php

namespace lib\Blog;

use lib\Blog\Entity\BlogItem;
use lib\Repository\DbSql;
use PDO;

/**
 * Class BlogDataRepository - handles database operations
 */
class BlogDataRepository
{

    /** @var DbSql */
    private $dbHandler;

    /**
     * BlogDataRepository constructor.
     */
    public function __construct()
    {
        $this->dbHandler = new DbSql();
    }

    /**
     * Inserts data into database table using PDO prepared statement
     *
     * @param BlogItem $blogItem
     *
     * @return boolean
     */
    public function insertBlogData(BlogItem $blogItem)
    {
        $this->dbHandler->query("INSERT INTO `blog_items` (
                `user_name`, 
                `server_name`, 
                `entry_number`, 
                `url`, 
                `title`, 
                `description`, 
                `date`
                ) VALUES (
                :userName, 
                :serverName, 
                :entryNumber, 
                :url, 
                :title, 
                :description, 
                :date)");

        $this->dbHandler->bind(':userName', $blogItem->getUserName(), PDO::PARAM_STR);
        $this->dbHandler->bind(':serverName', $blogItem->getServerName(), PDO::PARAM_INT);
        $this->dbHandler->bind(':entryNumber', $blogItem->getEntryNumber(), PDO::PARAM_INT);
        $this->dbHandler->bind(':url', $blogItem->getUrl(), PDO::PARAM_STR);
        $this->dbHandler->bind(':title', $blogItem->getTitle(), PDO::PARAM_STR);
        $this->dbHandler->bind(':description', $blogItem->getDescription(), PDO::PARAM_STR);
        $this->dbHandler->bind(':date', $blogItem->getDate(), PDO::PARAM_STR);

        return $this->dbHandler->execute();
    }

    /**
     * Removes older data from database
     *
     * @param string $weeksNumber
     *
     * @return boolean
     */
    public function removeOlderData($weeksNumber)
    {
        $this->dbHandler->query("DELETE FROM `blog_items` WHERE `date` < DATE_SUB(NOW(), INTERVAL :weeksNumber WEEK)");
        $this->dbHandler->bind(':weeksNumber', $weeksNumber, PDO::PARAM_STR);

        return $this->dbHandler->execute();
    }

    /**
     * Retrieves data from database based on filterOptions specified into interface
     *
     * @param BlogDataFilter $filterOptions
     *
     * @return BlogItem[]
     */
    public function getFilteredData(BlogDataFilter $filterOptions)
    {
        $sql = "SELECT `blog_item_id`, `url`, `title`, `description`, `date` FROM `blog_items` %s ORDER BY blog_item_id DESC LIMIT :startLimit, :itemsPerPage" ;

        $whereCondition = $this->buildWhereCondition($filterOptions);

        $this->dbHandler->query(sprintf($sql, $whereCondition));
        $this->dbHandler->bind(':startLimit', (int)$filterOptions->getCurrentPage() * (int)BlogDataFilter::ITEMS_PER_PAGE, PDO::PARAM_INT);
        $this->dbHandler->bind(':itemsPerPage', (int)BlogDataFilter::ITEMS_PER_PAGE, PDO::PARAM_INT);

        $returnedRows = $this->dbHandler->getAll();
        $returnedBlogItems = [];

        if (!empty($returnedRows)) {
            foreach ($returnedRows as $itemRow) {
                $returnedBlogItems[] = (new BlogItem(
                    null,
                    null,
                    null,
                    $itemRow['url'],
                    $itemRow['title'],
                    $itemRow['description'],
                    $itemRow['date']
                    ))->withItemId($itemRow['blog_item_id']);
            }
        }

        return $returnedBlogItems;

    }

    /**
     * Returns total number of items from database based on filterOptions
     *
     * @param BlogDataFilter $filterOptions
     *
     * @return int
     */
    public function getTotalNumberOfRecords(BlogDataFilter $filterOptions)
    {
        $sql = "SELECT COUNT(`blog_item_id`) as `total_records` FROM `blog_items` %s" ;

        $whereCondition = $this->buildWhereCondition($filterOptions);

        $this->dbHandler->query(sprintf($sql, $whereCondition));

        return $this->dbHandler->getRow()['total_records'];
    }

    /**
     * @param BlogDataFilter $filterOptions
     * @return string
     */
    private function buildWhereCondition(BlogDataFilter $filterOptions)
    {
        $whereCondition = " WHERE 1=1  ";

        if (!is_null($filterOptions->getUserName())) {
            $whereCondition .= ' AND `user_name` = ' . $this->dbHandler->quote($filterOptions->getUserName());
        }

        if (!is_null($filterOptions->getServerName())) {
            $whereCondition .= ' AND `server_name` = ' . $this->dbHandler->quote($filterOptions->getServerName(),
                    PDO::PARAM_INT);
        }

        if (!is_null($filterOptions->getEntryNumber())) {
            $whereCondition .= ' AND `entry_number` = ' . $this->dbHandler->quote($filterOptions->getEntryNumber(),
                    PDO::PARAM_INT);
        }

        if (!is_null($filterOptions->getDate())) {
            $whereCondition .= ' AND DATE(`date`) = ' . $this->dbHandler->quote($filterOptions->getDate());
        }
        return $whereCondition;
    }

}
