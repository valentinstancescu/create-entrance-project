<?php


namespace lib\Blog;


use lib\Blog\Entity\BlogItem;

/**
 * Class BlogItemBuilder - builds Entity class to be stored into database
 */
class BlogItemBuilder
{

    public function buildBlogItem(BlogData $blogParsedItem)
    {
        $itemUrl = $blogParsedItem->getUrl();
        $parsedItemUrl = $this->parseItemUrl($itemUrl);

        return new BlogItem(
            $parsedItemUrl['userName'],
            $parsedItemUrl['serverName'],
            $parsedItemUrl['entryName'],
            $itemUrl,
            $blogParsedItem->getTitle(),
            $blogParsedItem->getDescription(),
            $blogParsedItem->getDate());
    }

    /**
     * @param string $itemUrl
     * @return array
     */
    private function parseItemUrl($itemUrl)
    {
        preg_match('/^http:\/\/([\w]+).([\w?]+).fc2.com\/blog-entry-([0-9]+).html/', $itemUrl, $matches);
        $unParsedServerName = !empty($matches[2]) ? $matches[2] : '';
        $serverName = str_replace('blog', '', $unParsedServerName);

        return [
            'userName' => (!empty($matches[1])) ?$matches[1] : null,
            'serverName' => (!empty($serverName)) ? $serverName : null,
            'entryName' => (!empty($matches[3])) ?$matches[3] : null,
        ];
    }
}