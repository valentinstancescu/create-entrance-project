<?php

namespace lib\Configuration;

/**
 * Class ConfigParser - parse ini file.
 */
class ConfigParser
{
    const DATABASE_KEY = 'database';
    const BLOG_KEY = 'blog';

    private $iniFile;

    /**
     * ConfigParser constructor.
     */
    public function __construct()
    {
        $this->iniFile = parse_ini_file(dirname(__DIR__) . '/../../config/production.ini', true);
    }

    /**
     * @param string $sectionName
     * @return array
     */
    public function getConfigBySection($sectionName)
    {
        return $this->iniFile[$sectionName];
    }

}
