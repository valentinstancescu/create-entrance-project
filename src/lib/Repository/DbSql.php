<?php

namespace lib\Repository;

use lib\Configuration\ConfigParser;
use PDO;
use PDOException;

/**
 * Class DbSql - handles database connection and offers basic database operations via PDO
 */
class DbSql{

    /** @var PDO */
    private $dbHandler;

    /** @var string */
    private $error;

    /** @var \PDOStatement */
    private $statement;

    /** @var array */
    private $dbConfig;

    /**
     * DbSql constructor.
     */
    public function __construct(){

        $configParser = new ConfigParser();
        $this->dbConfig = $configParser->getConfigBySection(ConfigParser::DATABASE_KEY);

        //dsn for mysql
        $dsn = "mysql:host=" . $this->dbConfig['host'] . ";dbname=" . $this->dbConfig['database'] . ";charset=utf8";
        $options = [
            PDO::ATTR_PERSISTENT    => true,
            PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION,
        ];

        try{
            $this->dbHandler = new PDO($dsn, $this->dbConfig['user'], $this->dbConfig['password'], $options);
        } catch (PDOException $e){
            $this->error = $e->getMessage();
        }

    }

    /**
     * @param string $query
     */
    public function query($query){
        $this->statement = $this->dbHandler->prepare($query);
    }

    /**
     * @param string $param
     * @param mixed $value
     * @param mixed|null $type
     */
    public function bind($param, $value, $type = null){
        if(is_null($type)){
            switch (true){
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->statement->bindValue($param, $value, $type);
    }

    /**
     * @return bool
     */
    public function execute(){
        return $this->statement->execute();
    }

    /**
     * @return array
     */
    public function getAll(){
        $this->execute();
        return $this->statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return mixed
     */
    public function getRow(){
        $this->execute();
        return $this->statement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @return int
     */
    public function rowCount(){
        return $this->statement->rowCount();
    }

    /**
     * @return string
     */
    public function lastInsertId(){
        return $this->dbHandler->lastInsertId();
    }

    /**
     * @param string $param
     * @param int $type
     * @return string
     */
    public function quote($param, $type = PDO::PARAM_STR)
    {
        return$this->dbHandler->quote($param, $type);
    }

}
