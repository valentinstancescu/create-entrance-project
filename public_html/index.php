<?php

use lib\Smarty\HomeController;

require_once(dirname(__DIR__) . '/app/init.php');

$homeController = new HomeController();
$serverGetRequest = [];

if (!(empty($_GET))) {
    $serverGetRequest = $_GET;

    if(
        !empty($serverGetRequest['user_name']) ||
        !empty($serverGetRequest['server_name']) ||
        !empty($serverGetRequest['entry_number']) ||
        !empty($serverGetRequest['date'])
    ) { //current_page is always sent, and I don't want to set cookie just for it
        $homeController->setCookie($serverGetRequest);
    }
} elseif ((!empty($_COOKIE[HomeController::FILTER_COOKIE_NAME]))){
    $serverGetRequest = $homeController->decodeCookieContent($_COOKIE[HomeController::FILTER_COOKIE_NAME]);
}
$homeController->display($serverGetRequest);