##Create entrance project

The project reads FC2BLOG’s RSS updates from [http://blog.fc2.com/newentry.rdf] store them into database and offers a query page where items can be searched.

---

## How to use it

You’ll start by accessing the root path of the project

1. Home page displays a form with 4 inputs from which data can be filtered
2. By default latest 10 items queried from the RSS files will be shown
3. You can search be any criteria mentioned into form fields
4. After clicking **Show items** button, data will be shown below the button.
5. Pagination is also available, together with a total number of items display.

---

## Installation of the application

1. Clone project from Bitbucket: `git clone https://valentinstancescu@bitbucket.org/valentinstancescu/create-entrance-project.git`
2. Make sure the server where project is deployed runs: `PHP >=5.3`, `Apache2`, `MySql >= 5.5`.
3. Php modules that must to be enabled: `PDO`, `cUrl`.
4. Application also requires `COMPOSER` to be installed.
5. Copy files on the server, making `public_html` folder as root of the project.
6. Run `composer install` to install external libraries as _Smarty Templates_
7. Create `templates_c` folder near `public_html` folder in order for _Smarty_ to save compiled templates. Make folder writable.
8. Create database table:
   ``CREATE TABLE `blog_items` (
       `blog_item_id` INT(11) NOT NULL AUTO_INCREMENT,
       `user_name` VARCHAR(100) DEFAULT NULL,
       `server_name` INT(6) DEFAULT NULL,
       `entry_number` INT(6) DEFAULT NULL,
       `url` VARCHAR(100) DEFAULT NULL,
       `title` VARCHAR(255) DEFAULT NULL,
       `description` TEXT,
       `date` DATETIME DEFAULT NULL,
       PRIMARY KEY (`blog_item_id`),
       KEY `USERNAME` (`user_name`),
       KEY `SERVERNAME` (`server_name`),
       KEY `ENTRYNAME` (`entry_number`),
       KEY `DATE` (`date`)
     ) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8``
9. Setup cron job to take RSS data `crontab -e` and `*/5 * * * * php /home/exam0121/cron/get_data.php`. Cron job will run every 5 minutes. Save crontab.
10. Setup cron job to delete older database entries `crontab -e` and `30 1 * * * php  /home/exam0121/cron/remove_old_data.php 2`. 
Cron job will run every day at 1:30 AM, and will remove entries older than 2 weeks. 
Cron parameter represents number of weeks.

---

## Folders/files and their roles

Short description of folders and files of the project

1. `\app\init.php` - holds the files that initialize the common data for all application.
2. `\config` - holds configuration files.
3. `\config\production.ini` - ini file used for production configuration. 
For multiple environments of the application, different ini files should be defined, such as: `development.ini` or `staging.ini`
4. `\cron` - holds end point files for cron jobs.
5. `\public_html` - holds entry points / public files of the project.
6. `\src` - holds sources and templates of the application.
7. `\src\lib` - holds folders and files for application logic.
8. `\src\lib\Blog` - module for RSS data handling. Contains service class that expose methods for data handling.
Also provides and Entity class and Repository class that handle database operations with parsed data.
9.  `\src\lib\Communication` - module that handles HTTP communication of the application.
10. `\src\lib\configuration` - module that parses the config file.
11. `\src\lib\Repository` - module that handles the Database connection details, and provides general methods for data manipulation.
12. `\src\lib\Smarty` - module for template and entry point handling
13. `\src\templates` - holds Smarty template files.
14. `\composer.json` - Composer file where external libraries are specified.

- External libraries used: Smarty Templates and Bootstrap CSS